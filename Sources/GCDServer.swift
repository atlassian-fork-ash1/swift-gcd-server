//
//  GCDServer.swift
//  HttpServer
//
//  Created by Nikolay Petrov on 6/27/16.
//  Copyright © 2016 Swift. All rights reserved.
//

#if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
    import Darwin
#elseif os(Linux)
    import Glibc
#endif

import Foundation
import Dispatch

enum GCDError: Error {
    case AddressError
    case NoAddressError
    case BindError
    case ListenError
}

public struct GCDConfiguration {
    public let port: UInt16

    public init(port: UInt16) {
        self.port = port
    }
}

public class GCDServer {
    public var connectedCallback: ((GCDConnection) -> Void)?
    public var disconnectedCallback: ((GCDConnection) -> Void)?

    private let configuration: GCDConfiguration

    fileprivate let queue = DispatchQueue(label: "gcdserver")
    private var source: DispatchSourceRead?
    fileprivate var serverSocket: Int32?

    fileprivate var connections: [UUID: GCDConnection] = [:]

    fileprivate let connectionAddress = UnsafeMutablePointer<sockaddr>.allocate(capacity: 1)
    fileprivate let connectionAddressLength = UnsafeMutablePointer<socklen_t>.allocate(capacity: 1)

    public init(configuration: GCDConfiguration) {
        self.configuration = configuration
    }

    deinit {
        connectionAddress.deallocate(capacity: 1)
        connectionAddressLength.deallocate(capacity: 1)
    }

    public func start() throws {
        var hints = addrinfo()
        memset(&hints, 0, MemoryLayout<addrinfo>.size)
        hints.ai_family = PF_UNSPEC
        hints.ai_flags = AI_PASSIVE
        #if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
            hints.ai_socktype = SOCK_STREAM
        #elseif os(Linux)
            hints.ai_socktype = Int32(SOCK_STREAM.rawValue)
        #endif

        let addresses = UnsafeMutablePointer<UnsafeMutablePointer<addrinfo>?>.allocate(capacity: 1)
        defer {
            addresses.deallocate(capacity: 1)
        }

        if getaddrinfo("0.0.0.0", "\(configuration.port)", &hints, addresses) != 0 {
            throw GCDError.AddressError
        }
        defer {
            if let address = addresses.pointee {
                freeaddrinfo(address)
            }
        }

        guard let address = addresses.pointee?.pointee else { throw GCDError.NoAddressError }
        let serverSocket = socket(address.ai_family, address.ai_socktype, address.ai_protocol)

        if fcntl(serverSocket, F_SETFL, O_NONBLOCK) == -1 {
            print("[gcd-tcp] server fcntl failed")
        }

        var enableSetting: Int32 = 1
        #if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
            if setsockopt(serverSocket, SOL_SOCKET, SO_NOSIGPIPE, &enableSetting, UInt32(MemoryLayout<Int>.size)) == -1 {
                print("[gcd-tcp] nosigpipe failed")
            }
        #endif

        if setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &enableSetting, UInt32(MemoryLayout<Int>.size)) == -1 {
            print("[gcd-tcp] server reuse failed")
        }

        if bind(serverSocket, UnsafePointer<sockaddr>(address.ai_addr), address.ai_addrlen) < 0 {
            throw GCDError.BindError
        }

        if listen(serverSocket, 128) < 0 {
            throw GCDError.ListenError
        }

        self.serverSocket = serverSocket

        source = DispatchSource.makeReadSource(fileDescriptor: serverSocket, queue: queue)
        source?.setEventHandler { [weak self] in self?.socketEvent() }
        source?.setCancelHandler { [weak self] in self?.socketCancel() }
        source?.resume()
    }

    public func stop() {
        source?.cancel()
    }
}

internal extension GCDServer {
    func socketEvent() {
        connectionAddress.initialize(to: sockaddr())
        connectionAddressLength.initialize(to: socklen_t())

        defer {
            connectionAddress.deinitialize()
            connectionAddressLength.deinitialize()
        }

        let connectionSocket = accept(self.serverSocket!, connectionAddress, connectionAddressLength)
        if connectionSocket < 0 {
            return
        }

        if fcntl(connectionSocket, F_SETFL, O_NONBLOCK) == -1 {
            print("[gcd-tcp] fcntl failed")
        }

        #if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
            var enableSetting: Int32 = 1
            if setsockopt(connectionSocket, SOL_SOCKET, SO_NOSIGPIPE, &enableSetting, UInt32(MemoryLayout<Int>.size)) == -1 {
                print("[gcd-tcp] nosigpipe failed")
            }
        #endif

        let connection = GCDConnection(server: self, socket: connectionSocket)
        self.connections[connection.uuid] = connection

        self.connectedCallback?(connection)

        connection.readSource.resume()
    }

    func socketCancel() {
        guard let serverSocket = serverSocket else { return }

        close(serverSocket)
    }

    func disconnectEvent(connection: GCDConnection) {
        queue.async {
            self.connections.removeValue(forKey: connection.uuid)
            self.disconnectedCallback?(connection)
        }
    }
}
